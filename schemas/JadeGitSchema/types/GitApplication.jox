<Class name="GitApplication" id="f1f11d2a-b435-44cd-9809-e74b83ce9d98">
    <superclass name="RootSchemaApp"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="_usergui"/>
    <Constant name="Event_SetupSchema" id="d8556ef2-1063-45bc-ad8c-d3ac2d40ea4f">
        <constantUsages>
            <GlobalConstant name="User_Base_Event"/>
        </constantUsages>
        <source>User_Base_Event</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="Timer_InteropWait" id="f32cfa46-e62a-43ea-814e-b6bcd502c2c4">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <ImplicitInverseRef name="developer" id="0ef70b7d-79dc-4fb1-ae8e-31b68dd24df2">
        <embedded>true</embedded>
        <type name="Process"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="user" id="0913ae8d-3f6e-476d-800f-5c7c6e0f5c6e">
        <embedded>true</embedded>
        <type name="User"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <JadeMethod name="explorerFinalize" id="00e081f2-0deb-4436-a4cb-bc1997be9905">
        <updating>true</updating>
        <source>explorerFinalize() updating, protected;

begin
	// Invoke callback method to indicate session has been stopped
	if developer &lt;&gt; null then
	//	explorerStopped( developer );		// TODO: No longer required?
	endif;
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="explorerGlobalHandler" id="25a7efd9-f278-4103-8a17-c7f724bb5244">
        <source>explorerGlobalHandler(ex: Exception): Integer protected;

begin
	if ex.isKindOf( GitException ) then
		app.msgBox( ex.text, "Jade-Git Error", MsgBox_Stop_Icon );
		return Ex_Abort_Action;
	endif;

	return Ex_Pass_Back;
end;
</source>
        <access>protected</access>
        <Parameter name="ex">
            <type name="Exception"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="explorerInitialize" id="c7c815fb-b7ab-479c-bae0-c02ec860ea98">
        <updating>true</updating>
        <source>explorerInitialize(developerProcess: Process) updating, protected;

vars
	userName : String;
	
begin
	// Handle direct vs IDE startup
	if developerProcess &lt;&gt; null then
		// Arm exception handler for startup issues
		on Exception do explorerInitializeHandler(exception, developerProcess);
				
		// Invoke callback method to indicate session is running
		userName := explorerStarted(developerProcess);
		
	else
		userName := app.userName;
	endif;
	
	// Initialise user
	user := User@get(userName);
	
	// Set session user
	sessionSignOn(user);
		
	if developerProcess &lt;&gt; null then
		// Listen for IDE process being deleted
		beginNotification(developerProcess, Object_Delete_Event, Response_Cancel, null);
		
		// Listen for user events caused against current process (by IDE to handle interactive functionality)
		beginNotification(process, Any_User_Event, Response_Continuous, null);
		
		// Complete IDE sign-on, supplying user
		explorerSignOn(developerProcess, user);
	endif;
	
	// Remember developer process
	developer := developerProcess;
	
	// Arm global exception handler
	on Exception do explorerGlobalHandler(exception) global;
	
	// Start explorer, disabling ability to close if opened with IDE (app is closed when IDE is)
	ExplorerForm@open(user).setAllowClose(developer = null).show();
end;</source>
        <access>protected</access>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="explorerInitializeHandler" id="fbc68fc9-4e86-42ba-91ea-d865e823aecc">
        <source>explorerInitializeHandler(ex: Exception; developerProcess: Process): Integer protected;

begin
	// TODO: Can any exceptions be handled?

	// Invoke callback method to indicate session startup has stalled
	explorerStalled( developerProcess, ex.errorCode );
	
	// Invoke default handler to log basic stack dump
	ex.defaultHandler;
	
	// Abort startup
	return Ex_Abort_Action;
end;</source>
        <access>protected</access>
        <Parameter name="ex">
            <type name="Exception"/>
        </Parameter>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <ExternalMethod name="explorerReply" id="3c546cd6-feb6-4e64-98cc-131c6bc61b9e">
        <entrypoint>jadegit_explorer_reply</entrypoint>
        <executionLocation>server</executionLocation>
        <library name="jadegitscm"/>
        <source>explorerReply(developerProcess: Process; params: ParamListType) is jadegit_explorer_reply in jadegitscm serverExecution;
</source>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
        <Parameter name="params">
            <type name="ParamListType"/>
        </Parameter>
    </ExternalMethod>
    <ExternalMethod name="explorerSignOn" id="91bca8ae-2ea2-440d-b958-6f9a8dae38eb">
        <entrypoint>jadegit_explorer_signon</entrypoint>
        <executionLocation>server</executionLocation>
        <library name="jadegitscm"/>
        <source>explorerSignOn(developerProcess: Process; user: User) is jadegit_explorer_signon in jadegitscm protected, serverExecution;
</source>
        <access>protected</access>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
        <Parameter name="user">
            <type name="User"/>
        </Parameter>
    </ExternalMethod>
    <ExternalMethod name="explorerStalled" id="824f31e0-af33-45ab-ac76-089b91b3e630">
        <entrypoint>jadegit_explorer_stalled</entrypoint>
        <executionLocation>server</executionLocation>
        <library name="jadegitscm"/>
        <source>explorerStalled(developerProcess: Process; errorCode: Integer) is jadegit_explorer_stalled in jadegitscm protected, serverExecution;</source>
        <access>protected</access>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
        <Parameter name="errorCode">
            <type name="Integer"/>
        </Parameter>
    </ExternalMethod>
    <ExternalMethod name="explorerStarted" id="3c725bd9-6653-4284-842c-eeff4abf770b">
        <entrypoint>jadegit_explorer_started</entrypoint>
        <executionLocation>server</executionLocation>
        <library name="jadegitscm"/>
        <source>explorerStarted(developerProcess: Process): String is jadegit_explorer_started in jadegitscm protected, serverExecution;</source>
        <access>protected</access>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="explorerStopped" id="418b664b-269b-483e-9644-333ea3880c51">
        <entrypoint>jadegit_explorer_stopped</entrypoint>
        <executionLocation>server</executionLocation>
        <library name="jadegitscm"/>
        <source>explorerStopped(developerProcess: Process) is jadegit_explorer_stopped in jadegitscm protected, serverExecution;</source>
        <access>protected</access>
        <Parameter name="developerProcess">
            <type name="Process"/>
        </Parameter>
    </ExternalMethod>
    <JadeMethod name="getUserProfile" id="7e9f6aa3-d5eb-414c-b3d5-4eb7eedf5e76">
        <source>getUserProfile(): JadeUserProfile;

begin
	return rootSchema.getUserProfile(user.name).JadeUserProfile;
end;
</source>
        <ReturnType>
            <type name="JadeUserProfile"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="initialize" id="7fb69ae4-880b-48d2-806c-b4a0f7295fe6">
        <updating>true</updating>
        <source>initialize() updating;

begin
	inheritMethod();
	
	// Ensure root has been instantiated
	Root@get();
end;
</source>
    </JadeMethod>
    <ExternalMethod name="sessionSignOn" id="a7687571-d8f1-471c-9423-74cca18887fb">
        <entrypoint>jadegit_session_signon</entrypoint>
        <library name="jadegitscm"/>
        <source>sessionSignOn(user: User) is jadegit_session_signon in jadegitscm protected;</source>
        <access>protected</access>
        <Parameter name="user">
            <type name="User"/>
        </Parameter>
    </ExternalMethod>
    <JadeMethod name="sysNotification" id="9a55578b-6002-4a15-898c-64e58231b195">
        <updating>true</updating>
        <source>sysNotification(eventType: Integer; theObject: Object; eventTag: Integer) updating;

vars

begin
	if eventType = Object_Delete_Event and theObject = developer then
		terminate;
	endif;
end;
</source>
        <Parameter name="eventType">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="theObject">
            <type name="Object"/>
        </Parameter>
        <Parameter name="eventTag">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="userNotification" id="946e67b8-cf31-4588-926d-c2e78abcea12">
        <updating>true</updating>
        <source>userNotification(eventType: Integer; theObject: Object; eventTag: Integer; userInfo: Any) updating;

begin
	// Handle interactive events
	if theObject = process then
		if eventType = Event_SetupSchema then
			explorerReply(developer, GitSetupSchemaDialog@open(null).setSchemaName(userInfo.String).showModal().Boolean);
		endif;
	endif;
end;
</source>
        <Parameter name="eventType">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="theObject">
            <type name="Object"/>
        </Parameter>
        <Parameter name="eventTag">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="userInfo">
            <type name="Any"/>
        </Parameter>
    </JadeMethod>
</Class>
