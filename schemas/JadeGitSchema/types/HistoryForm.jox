<GUIClass name="HistoryForm" id="392381df-01b9-4478-b4f4-f358feb1c80a">
    <superclass name="GitForm"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <Constant name="TopupEvent" id="22121d23-7eda-4814-a16a-7e0177ec891b">
        <constantUsages>
            <GlobalConstant name="User_Base_Event"/>
        </constantUsages>
        <source>User_Base_Event</source>
        <type name="Integer"/>
    </Constant>
    <ImplicitInverseRef name="revwalk" id="110cdeca-81b8-43ae-a075-ad8a0e389613">
        <embedded>true</embedded>
        <type name="RevisionWalker"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="tblCommits" id="63dc6e82-5a36-4be7-9c54-6bd88462199a">
        <embedded>true</embedded>
        <type name="Table"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="commits" id="453ab635-5b13-471a-87a3-ec3ec571b2db">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="ObjectArray"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <JadeMethod name="delete" id="99f759ec-63a1-4ead-807f-f6823147d3d4">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>delete() updating, clientExecution;

begin
	endNotificationForSubscriber(self);
	delete revwalk;
end;
</source>
    </JadeMethod>
    <JadeMethod name="load" id="2ef7aa32-ac29-4953-96d2-169ad41e417f">
        <controlMethod name="Form::load"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>load() updating;

begin
	inheritMethod();

	centreWindow();

	tblCommits.accessSheet(1).marginLeft := 2;
	tblCommits.accessedSheet.marginRight := 2;
	tblCommits.accessedSheet.gridRight	 := false;
	
	tblCommits.columnWidth[3] := 115;
	tblCommits.columnWidth[4] := 60;
end;
</source>
    </JadeMethod>
    <JadeMethod name="populate" id="b4f57104-15d6-494b-b1f7-ed98241248fd">
        <updating>true</updating>
        <source>populate(branch: Branch): Boolean updating, protected;

begin
	commits.clear();
	delete revwalk;
	
	revwalk := RevisionWalker@new(branch.repo());
	
	revwalk.pushRef(branch.name);
	
	return inheritMethod(branch);
end;
</source>
        <access>protected</access>
        <Parameter name="branch">
            <type name="Branch"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblCommits_populate" id="40e40fe7-d02f-4680-ab09-5e951bb6076d">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblCommits_populate(table: Table input; object: Object):Boolean updating, protected;
	
begin
	tblCommits.setCellText(1, 1, "Message	Author	Date	ID");
	if topup() then
		beginNotification(self, TopupEvent, Response_Continuous, null);
	endif;
	tblCommits.displayCollection(commits, false, null, null);
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblCommits_displayRow" id="5e694023-804e-4804-8d76-247cae49176b">
        <controlMethod name="Table::displayRow"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblCommits_displayRow(table: Table input; theSheet: Integer; commit: Commit; theRow: Integer; bcontinue: Boolean io):String updating;

vars
	when : TimeStamp;

begin
	if commit = commits.last then
		causeEvent(TopupEvent, true, null);
	endif;
	
	when := commit.author.when.asUTCTimeStamp().utcToLocalTime;

	return commit.summary &amp; Tab &amp;
		   commit.author.name &amp; Tab &amp; 
		   when.date.shortFormat() &amp; " " &amp; when.time.String &amp; Tab &amp; 
		   commit.id[1:8];
end;
</source>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
        <Parameter name="theSheet">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="commit">
            <type name="Commit"/>
        </Parameter>
        <Parameter name="theRow">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="bcontinue">
            <usage>io</usage>
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblCommits_mouseHover" id="3ff01629-2548-42a3-8b43-877d0fcfa913">
        <controlMethod name="Table::mouseHover"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblCommits_mouseHover(table: Table input; button: Integer; shift: Integer; x: Real; y: Real) updating;

vars
	r, c   : Integer;
	commit : Commit;
	
begin
	table.getCellFromPosition(x, y, r, c);
	if r &lt;= table.fixedRows then
		table.bubbleHelp := null;
		return;
	endif;
	
	commit := table.accessRow(r).itemObject.Commit;
	
	table.bubbleHelp :=
	"Commit:		" &amp; commit.id &amp; CrLf &amp;
	"Author:		" &amp; commit.author.name &amp; " &lt;" &amp; commit.author.email &amp; "&gt;" &amp; CrLf &amp;
	"Author Date:	" &amp; commit.author.when.String &amp; CrLf &amp;
	"Committer:	" &amp; commit.committer.name &amp; " &lt;" &amp; commit.committer.email &amp; "&gt;" &amp; CrLf &amp;
	"Commit Date:	" &amp; commit.committer.when.String &amp; CrLf &amp; CrLf &amp;
						commit.message;
end;
</source>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
        <Parameter name="button">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="shift">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="x">
            <type name="Real"/>
        </Parameter>
        <Parameter name="y">
            <type name="Real"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="topup" id="5a2c8985-5b3d-4ec8-80cf-798c839bca99">
        <source>topup(): Boolean protected;

vars
	count : Integer;
	commit : Commit;
	
begin
	count := 100;
	while count &gt; 0 and revwalk.next(commit) do
		commits.add(commit);
		count -= 1;
	endwhile;
	return count = 0;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="userNotify" id="b6ab78e6-56f1-4126-9d14-b9a25b7560b4">
        <controlMethod name="Form::userNotify"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>userNotify(eventType: Integer; theObject: Object; eventTag: Integer; userInfo: Any) updating;

begin
	if eventType = TopupEvent then
		if not topup() then
			endNotification(self, TopupEvent);
		endif;
		tblCommits.refreshEntries(tblCommits.listObject());
	endif;
end;
</source>
        <Parameter name="eventType">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="theObject">
            <type name="Object"/>
        </Parameter>
        <Parameter name="eventTag">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="userInfo">
            <type name="Any"/>
        </Parameter>
    </JadeMethod>
</GUIClass>
